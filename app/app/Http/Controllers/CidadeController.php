<?php

namespace App\Http\Controllers;
use App\Cidade;
class CidadeController extends Controller{

    public function index(){
        $cidades = Cidade::all();
        return response()
        ->json($cidades);
    }

}
