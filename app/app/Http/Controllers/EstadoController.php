<?php

namespace App\Http\Controllers;
use App\Estado;
class EstadoController extends Controller{

    public function index(){
        $estados = Estado::all();
        return response()
        ->json($estados);
    }

}
