<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return array('services'=>array(
        '/cidades',
        '/estados'
    ));
});

$router->get('cidades/', 'CidadeController@index');
$router->get('estados/', 'EstadoController@index');