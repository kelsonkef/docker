FROM php:7.1-apache

RUN apt-get update && apt-get install -y libpq-dev

RUN docker-php-ext-install pdo pdo_pgsql

RUN a2enmod rewrite

COPY ./app/public /var/www/html/
COPY ./app/ /var/www/

ENV APP_ENV=local \
    APP_DEBUG=false \
    APP_KEY=key \
    APP_TIMEZONE=UTC \
    DB_CONNECTION=pgsql \
    DB_HOST=ec2-174-129-18-42.compute-1.amazonaws.com \
    DB_PORT=5432 \
    DB_DATABASE=dj2hfs3mj42jh \
    DB_USERNAME=bpoyesydlzbyad \
    DB_PASSWORD=01fccf02b5ad5e85162a27bbcb5b71cc90149d193d30ce19507c7a496f3da27f

CMD sed -i "s/80/$PORT/g" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf \
    && docker-php-entrypoint apache2-foreground
